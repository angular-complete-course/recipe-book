import { Component } from '@angular/core';
import { OnInit } from '@angular/core/';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  // currentMenu = 'recipies';
  // onNavigate(clickedItem: string) {
  //   this.currentMenu = clickedItem;
  // }

  ngOnInit() {
    firebase.initializeApp({
      apiKey: "AIzaSyDYk7mXblfIGOEc7Gum7RW902Ooe6ljm4c",
      authDomain: "angular-1576c.firebaseapp.com"
    });
  }
}
