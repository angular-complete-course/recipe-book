import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { error } from 'util';

@Injectable()
export class AuthService {
    constructor(private router: Router) { }
    token: string;
    signUpUser(email: string, password: string) {
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .catch(
            error => {
                alert(error.message);
            }

            )
    }

    signInUser(email: string, password: string) {
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(
            response => {
                this.router.navigate(['/']);
                firebase.auth().currentUser.getIdToken()
                    .then(
                    (token: string) => this.token = token
                    )
            }
            )
            .catch(
            error => {
                alert(error.message);
            }
            )
    }

    getToken() {
        firebase.auth().currentUser.getIdToken()
            .then(
            (token: string) => this.token = token
            );
        return this.token;
    }

    isAuthenticated() {
        return this.token != null;
    }

    logout() {
        firebase.auth().signOut();
        this.token = null;
        this.router.navigate(['/']);
    }
}