import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipieStarComponent } from './recipie-star.component';

describe('RecipieStarComponent', () => {
  let component: RecipieStarComponent;
  let fixture: ComponentFixture<RecipieStarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipieStarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipieStarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
