import { AuthService } from './../auth/auth.service';
import { Http, Headers, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { ShoppingListService } from './../shopping-list/shopping-list.service';
import { Ingredient } from './../shared/ingredient.model';
import { Injectable } from '@angular/core';
import { Recipie } from './recipie.model';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';


@Injectable()
export class RecipieService {
    recipeChanged = new Subject<Recipie[]>();

    private recipies: Recipie[] = [
        new Recipie('A test Recipie',
            'This is simply a test',
            'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg',
            [
                new Ingredient('Meat', 1),
                new Ingredient('French Fries', 20)
            ]
        ),
        new Recipie('A test Recipie2',
            'This is simply a test2',
            'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg',
            [
                new Ingredient('Buns', 2),
                new Ingredient('Meat', 1),
            ]

        )
    ];

    getRecipies() {
        return this.recipies.slice(); // new COPY array
    }

    getRecipie(index: number) {
        return this.recipies[index];
    }

    constructor(private shoppingListService: ShoppingListService, private http: Http, private authService: AuthService) { }

    storeRecipies() {
        const token = this.authService.getToken();
        const headers = new Headers({
            'Content-Type': 'applicaiton/json'
        });
        return this.http.put('https://angular-1576c.firebaseio.com/recipies.json?auth=' + token, this.getRecipies(), { headers: headers })
    }

    getFromServerRecipies() {
        const token = this.authService.getToken();

        this.http.get('https://angular-1576c.firebaseio.com/recipies.json?auth=' + token)
            .map(
            (response: Response) => {
                const recipies: Recipie[] = response.json();
                for (let recipie of recipies) {
                    if (!recipie['ingredients']) {
                        recipie['ingredients'] = [];
                    }
                }
                return recipies;
            }
            )
            .subscribe(
            (recipies: Recipie[]) => {
                this.setRecipies(recipies);
            }
            )

    }

    setRecipies(recipies: Recipie[]) {
        this.recipies = recipies;
        this.recipeChanged.next(this.recipies.slice());
    }

    addIngredietsToShoppingList(ingredients: Ingredient[]) {
        this.shoppingListService.addIngredients(ingredients);
    }
    addRecipe(recipie: Recipie) {
        this.recipies.push(recipie);
        this.recipeChanged.next(this.recipies.slice());
    }

    updateRecipe(index: number, newRecipie: Recipie) {
        this.recipies[index] = newRecipie;
        this.recipeChanged.next(this.recipies.slice());
    }

    deleteRecipe(index: number) {
        if (this.authService.isAuthenticated()) {
            this.recipies.splice(index, 1);
            this.recipeChanged.next(this.recipies.slice());
        }

    }
}
