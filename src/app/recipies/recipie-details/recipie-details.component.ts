import { RecipieService } from './../recipie.service';
import { Recipie } from './../recipie.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-recipie-details',
  templateUrl: './recipie-details.component.html',
  styleUrls: ['./recipie-details.component.css']
})
export class RecipieDetailsComponent implements OnInit {
  recipie: Recipie;
  id: number;
  constructor(private recipieService: RecipieService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.recipie = this.recipieService.getRecipie(this.id);
      }
    );
  }

  onAddtoShoppingList() {
    this.recipieService.addIngredietsToShoppingList(this.recipie.ingredients);
  }

  onEditRecipie() {
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  onDeleteRecipie() {
    this.recipieService.deleteRecipe(this.id);
    this.router.navigate(['/recipies'], {relativeTo: this.route});
  }
}
