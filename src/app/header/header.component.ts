import { AuthService } from './../auth/auth.service';
import { Component, EventEmitter, Output } from '@angular/core';
import { RecipieService } from '../recipies/recipie.service';
import { Response } from '@angular/http';
@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent {
    constructor(private recipieService: RecipieService, private authService: AuthService) { }

    onSave() {
        this.recipieService.storeRecipies()
            .subscribe(
            (response: Response) =>
                console.log(response)

            );
    }

    onLoadFromServer() {
        this.recipieService.getFromServerRecipies();

    }
}